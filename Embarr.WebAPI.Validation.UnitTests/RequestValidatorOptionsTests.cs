﻿using System.Net;
using NUnit.Framework;
using Should;

namespace Embarr.WebAPI.Validation.UnitTests
{
    [TestFixture]
    public class RequestValidatorOptionsTests
    {
        [Test]
        public void Should_Set_Default_StatusCode_To_BadRequest()
        {
            // Act
            var requestValidatorOptions = new RequestValidatorOptions();

            // Assert
            requestValidatorOptions.HttpStatusCode.ShouldEqual(HttpStatusCode.BadRequest);
        }
    }
}