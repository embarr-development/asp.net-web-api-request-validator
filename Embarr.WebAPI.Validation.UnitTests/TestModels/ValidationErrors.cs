﻿using System.Collections.Generic;

namespace Embarr.WebAPI.Validation.UnitTests.TestModels
{
    public class ValidationErrors
    {
        public string Property { get; set; }
        public string FlattenedErrorMessage1 { get; set; }
        public string FlattenedErrorMessage2 { get; set; }
        public List<ErrorMessage> ErrorMessages { get; set; }

        public ValidationErrors()
        {
            ErrorMessages = new List<ErrorMessage>();
        }
    }
}