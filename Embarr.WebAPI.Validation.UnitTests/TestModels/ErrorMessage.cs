﻿namespace Embarr.WebAPI.Validation.UnitTests.TestModels
{
    public class ErrorMessage
    {
        public int ErrorNumber { get; set; }
        public string Message { get; set; }
    }
}