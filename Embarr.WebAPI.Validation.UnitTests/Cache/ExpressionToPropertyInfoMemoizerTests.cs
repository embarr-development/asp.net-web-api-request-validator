﻿using System;
using System.Linq.Expressions;
using System.Reflection;
using Embarr.WebAPI.Validation.Cache;
using Embarr.WebAPI.Validation.UnitTests.TestModels;
using NUnit.Framework;
using Should;

namespace Embarr.WebAPI.Validation.UnitTests.Cache
{
    [TestFixture]
    public class ExpressionToPropertyInfoMemoizerTests
    {
        [Test]
        public void Should_Return_Cache_Value_On_All_Subsequent_Calls_With_Same_Value()
        {
            // Arrange
            int timesCalledBeforeMemoization;
            var timesCalled = 0;

            Func<Expression, PropertyInfo> func = e =>
            {
                timesCalled++;
                return typeof (ValidationErrors).GetProperty("Property");
            };

            var memoizedFunc = func.Memoize();
            LambdaExpression expression = GetExpression(x => x.Property);

            // Act
            for (var i = 0; i < 10; i++)
            {
                func(expression);
            }

            timesCalledBeforeMemoization = timesCalled;
            timesCalled = 0;

            for (var i = 0; i < 10; i++)
            {
                memoizedFunc(expression);
            }

            // Assert
            timesCalledBeforeMemoization.ShouldEqual(10, "The test needs updating.");
            timesCalled.ShouldEqual(1, "The values are not being retrieved from the cache");
        }

        [Test]
        public void Should_Return_Cache_Value_On_All_Subsequent_Calls_With_By_Value()
        {
            // Arrange
            int timesCalledBeforeMemoization;
            var timesCalled = 0;

            Func<Expression, PropertyInfo> func = e =>
            {
                timesCalled++;
                return typeof(ValidationErrors).GetProperty("Property");
            };

            var memoizedFunc = func.Memoize();
            LambdaExpression getPropertyExpression = GetExpression(x => x.Property);
            LambdaExpression getApplicationAreaExpression = GetExpression(x => x.FlattenedErrorMessage1);

            // Act
            for (var i = 0; i < 10; i++)
            {
                func(getPropertyExpression);
            }

            for (var i = 0; i < 10; i++)
            {
                func(getApplicationAreaExpression);
            }

            timesCalledBeforeMemoization = timesCalled;
            timesCalled = 0;

            for (var i = 0; i < 10; i++)
            {
                memoizedFunc(getPropertyExpression);
            }

            for (var i = 0; i < 10; i++)
            {
                memoizedFunc(getApplicationAreaExpression);
            }

            // Assert
            timesCalledBeforeMemoization.ShouldEqual(20, "The test needs updating.");
            timesCalled.ShouldEqual(2, "The values are not being retrieved from the cache");
        }

        private Expression<Func<ValidationErrors, string>> GetExpression(Expression<Func<ValidationErrors, string>> property)
        {
            return property;
        }
    }
}