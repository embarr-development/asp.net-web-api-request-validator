﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Web.Http.ModelBinding;
using Embarr.WebAPI.Validation.Mappers;
using Embarr.WebAPI.Validation.UnitTests.TestModels;
using NUnit.Framework;
using Should;

namespace Embarr.WebAPI.Validation.UnitTests.Mappers
{
    [TestFixture]
    public class FlattenedErrorMessageMapperTests
    {
        private FlattenedErrorMessageMapper<ValidationErrors> flattenedErrorMessageMapper;

        [SetUp]
        public void SetUp()
        {
            flattenedErrorMessageMapper = new FlattenedErrorMessageMapper<ValidationErrors>(x => x.Property);
        }

        public class Constructor : ErrorMessageMapperTests
        {
            [Test]
            public void Should_Throw_Exception_If_ErroredPropertyMap_Is_Null()
            {
                // Act
                var exception = Assert.Throws<ArgumentException>(() => new FlattenedErrorMessageMapper<ValidationErrors>(null));

                // Assert
                exception.Message.ShouldEqual("The errored property expression cannot be null");
            }
        }

        public class Map : FlattenedErrorMessageMapperTests
        {
            [Test]
            public void Should_Throw_Exception_If_ErroredPropertyMap_Is_Null()
            {
                // Act
                var exception = Assert.Throws<ArgumentException>(() => flattenedErrorMessageMapper.Map(null));

                // Assert
                exception.Message.ShouldEqual("The error message object property expression cannot be null");
            }

            [Test]
            public void Should_Add_Error_Message_Map_To_Private_Collection()
            {
                // Arrange
                Expression<Func<ValidationErrors, object>> errorMapPropertyExpression = x => x.FlattenedErrorMessage1;

                // Act
                var returnedInstance = flattenedErrorMessageMapper.Map(errorMapPropertyExpression);

                // Assert
                var errorMaps = ((ErrorMessageMapperBase<ValidationErrors, ValidationErrors>)returnedInstance).ErrorMaps;
                errorMaps.ShouldNotBeNull();
                errorMaps.Count.ShouldEqual(1);
                errorMaps[0].Property.ShouldEqual(errorMapPropertyExpression);
            }

            [Test]
            public void Should_Add_Multiple_Error_Message_Maps_To_Private_Collection()
            {
                // Arrange
                Expression<Func<ValidationErrors, object>> errorMapPropertyExpression = x => x.FlattenedErrorMessage1;
                Expression<Func<ValidationErrors, object>> errorMapPropertyExpression2 = x => x.FlattenedErrorMessage2;

                // Act
                flattenedErrorMessageMapper.Map(errorMapPropertyExpression);
                var returnedInstance = flattenedErrorMessageMapper.Map(errorMapPropertyExpression2);

                // Assert
                var errorMaps = ((ErrorMessageMapperBase<ValidationErrors, ValidationErrors>)returnedInstance).ErrorMaps;
                errorMaps.ShouldNotBeNull();
                errorMaps.Count.ShouldEqual(2);
                errorMaps[0].Property.ShouldEqual(errorMapPropertyExpression);
                errorMaps[1].Property.ShouldEqual(errorMapPropertyExpression2);
            }
        }

        public class AddAction : FlattenedErrorMessageMapperTests
        {
            [Test]
            public void Should_Add_Action()
            {
                // Arrange
                Action<List<ValidationErrors>> action = e => { };

                // Act

                var returnedInstance = flattenedErrorMessageMapper.AddAction(action);

                // Assert
                var actions = ((ErrorMessageMapperBase<ValidationErrors, ValidationErrors>)returnedInstance).Actions;
                actions.Count.ShouldEqual(1);
                actions[0].ShouldEqual(action);
            }

            [Test]
            public void Should_Add_Multiple_Actions()
            {
                // Arrange
                Action<List<ValidationErrors>> action1 = e => { };
                Action<List<ValidationErrors>> action2 = e => { };

                // Act

                flattenedErrorMessageMapper.AddAction(action1);
                var returnedInstance = flattenedErrorMessageMapper.AddAction(action2);

                // Assert
                var actions = ((ErrorMessageMapperBase<ValidationErrors, ValidationErrors>)returnedInstance).Actions;
                actions.Count.ShouldEqual(2);
                actions[0].ShouldEqual(action1);
                actions[1].ShouldEqual(action2);
            }
        }

        public class ToErrorMessage : FlattenedErrorMessageMapperTests
        {
            [Test]
            public void Should_Set_Last_Error_Message_Map_ToErrorMessage_To_True()
            {
                // Arrange
                Expression<Func<ValidationErrors, object>> errorMapPropertyExpression = x => x.FlattenedErrorMessage1;
                var returnedInstance = flattenedErrorMessageMapper.Map(errorMapPropertyExpression);

                // Act
                returnedInstance.ToErrorMessage();

                // Assert
                var errorMaps = ((ErrorMessageMapperBase<ValidationErrors, ValidationErrors>)returnedInstance).ErrorMaps;
                errorMaps.ShouldNotBeNull();
                errorMaps.Count.ShouldEqual(1);
                errorMaps[0].ToErrorMessage.ShouldBeTrue();
            }
        }

        public class ResolveUsing : FlattenedErrorMessageMapperTests
        {
            [Test]
            public void Should_Set_RsolveUsing_To_Passed_In_Action()
            {
                // Arrange
                var returnedInstance = flattenedErrorMessageMapper.Map(x => x.FlattenedErrorMessage1);
                Func<string, object> resolveUsingAction = x => null;

                // Act

                returnedInstance.ResolveUsing(resolveUsingAction);

                // Assert
                var errorMaps = ((ErrorMessageMapperBase<ValidationErrors, ValidationErrors>)returnedInstance).ErrorMaps;
                errorMaps.ShouldNotBeNull();
                errorMaps.Count.ShouldEqual(1);
                errorMaps[0].ToErrorMessage.ShouldBeFalse();
                errorMaps[0].Resolver.ShouldEqual(resolveUsingAction);
            }
        }

        public class CreateValidationErrorResponse : FlattenedErrorMessageMapperTests
        {
            [Test]
            public void Should_Return_Valid_Validation_Error_For_ModelState_Item()
            {
                // Arrange
                var value = new ModelState();
                value.Errors.Add(new ModelError("Name is required"));
                var modelState = new KeyValuePair<string, ModelState>("Name", value);
                flattenedErrorMessageMapper.ErrorMaps.Add(new ErrorMap<ValidationErrors>
                {
                    Property = x => x.FlattenedErrorMessage1,
                    ToErrorMessage = true
                });

                // Act
                var validationItem = flattenedErrorMessageMapper.CreateValidationErrorModel(new RequestValidatorOptions(), modelState);

                // Assert
                validationItem.Property.ShouldEqual("Name");
                validationItem.FlattenedErrorMessage1.ShouldEqual("Name is required");
            }
        }
    }
}