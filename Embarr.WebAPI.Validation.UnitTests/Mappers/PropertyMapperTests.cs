﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Reflection;
using Embarr.WebAPI.Validation.Mappers;
using Embarr.WebAPI.Validation.UnitTests.TestModels;
using NUnit.Framework;
using Should;

namespace Embarr.WebAPI.Validation.UnitTests.Mappers
{
    [TestFixture]
    public class PropertyMapperTests
    {
        private PropertyMapper<ValidationErrors> propertyMapper;

        [SetUp]
        public void SetUp()
        {
            propertyMapper = new PropertyMapper<ValidationErrors>();
        }

        public class MapPropertyName : PropertyMapperTests
        {
            [Test]
            public void Should_Throw_Exception_If_Property_Expression_Is_Null()
            {
                // Act
                var exception = Assert.Throws<ArgumentException>(() => propertyMapper.MapPropertyName(null));

                // Assert
                exception.Message.ShouldEqual("The property expression cannot be null");
            } 

            [Test]
            public void Should_Return_Property_Mapper_With_Property_Name_Func_Stored()
            {
                // Arrange
                Expression<Func<ValidationErrors, string>> propertyExpression = x => x.Property;

                // Act
                var returnedPropertyMapper = propertyMapper.MapPropertyName(propertyExpression);

                // Assert
                returnedPropertyMapper.ShouldEqual(propertyMapper);
                var erroredPropertyMap = typeof (PropertyMapper<ValidationErrors>).GetField("errorProperty", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(returnedPropertyMapper);
                erroredPropertyMap.ShouldEqual(propertyExpression);
            }
        }

        public class MapPropertyErrorMessagesCollection : PropertyMapperTests
        {
            [Test]
            public void Should_Throw_Exception_If_Property_Expression_Is_Null()
            {
                // Act
                var exception = Assert.Throws<ArgumentException>(() => propertyMapper.MapPropertyErrorMessagesCollection((Func<ValidationErrors, ICollection<ErrorMessage>>)null));

                // Assert
                exception.Message.ShouldEqual("The errorMessages expression cannot be null");
            }

            [Test]
            public void Should_Return_New_ErrorMessageMapper()
            {
                // Arrange
                Expression<Func<ValidationErrors, string>> propertyExpression = x => x.Property;
                Func<ValidationErrors, ICollection<ErrorMessage>> errorMessages = x => x.ErrorMessages;
                var returnedPropertyMapper = propertyMapper.MapPropertyName(propertyExpression);
               
                // Act
                var errorMessageMapper = propertyMapper.MapPropertyErrorMessagesCollection(errorMessages);

                // Assert
                errorMessageMapper.ShouldNotBeNull();
                var erroredPropertyMap = typeof(PropertyMapper<ValidationErrors>).GetField("errorProperty", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(returnedPropertyMapper);
                erroredPropertyMap.ShouldEqual(propertyExpression);
                var errorsMap = ((ErrorMessageMapper<ValidationErrors, ErrorMessage>) errorMessageMapper).ErrorMessagesMap;
                errorsMap.ShouldEqual(errorMessages);
            } 
        }

        public class FlattenErrorMessages : PropertyMapperTests
        {
            [Test]
            public void Should_Return_FlattenedErrorMessageMapper_Of_Correct_Generic_Type()
            {
                // Arrange
                Expression<Func<ValidationErrors, string>> propertyExpression = x => x.Property;
                propertyMapper.MapPropertyName(propertyExpression);

                // Act
                var flattenedErrorMessageMapper = propertyMapper.FlattenErrorMessages();

                // Assert
                flattenedErrorMessageMapper.ShouldNotBeNull();

                var erroredPropertyMapProperty = typeof(ErrorMessageMapperBase<ValidationErrors, ValidationErrors>).GetProperty("ErroredPropertyMap", BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
                var erroredPropertyMapValue = erroredPropertyMapProperty.GetValue(flattenedErrorMessageMapper);
                erroredPropertyMapValue.ShouldEqual(propertyExpression);
            }
        }
    }
}