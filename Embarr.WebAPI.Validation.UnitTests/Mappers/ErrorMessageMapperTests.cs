﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web.Http.ModelBinding;
using System.Web.Http.ValueProviders;
using Embarr.WebAPI.Validation.Mappers;
using Embarr.WebAPI.Validation.UnitTests.TestModels;
using NUnit.Framework;
using Should;

namespace Embarr.WebAPI.Validation.UnitTests.Mappers
{
    [TestFixture]
    public class ErrorMessageMapperTests
    {
        private ErrorMessageMapper<ValidationErrors, ErrorMessage> errorMessageMapper;

        [SetUp]
        public void SetUp()
        {
            errorMessageMapper = new ErrorMessageMapper<ValidationErrors, ErrorMessage>(x => x.Property, x => x.ErrorMessages);
        }

        public class Constructor : ErrorMessageMapperTests
        {
            [Test]
            public void Should_Throw_Exception_If_ErroredPropertyMap_Is_Null()
            {
                // Act
                var exception = Assert.Throws<ArgumentException>(() => new ErrorMessageMapper<ValidationErrors, ErrorMessage>(null, x => x.ErrorMessages));

                // Assert
                exception.Message.ShouldEqual("The errored property expression cannot be null");
            }

            [Test]
            public void Should_Throw_Exception_If_ErrorMessagesMap_Is_Null()
            {
                // Act
                var exception = Assert.Throws<ArgumentException>(() => new ErrorMessageMapper<ValidationErrors, ErrorMessage>(x => x.Property, null));

                // Assert
                exception.Message.ShouldEqual("The error messages expression cannot be null");
            }
        }

        public class Map : ErrorMessageMapperTests
        {
            [Test]
            public void Should_Throw_Exception_If_ErroredPropertyMap_Is_Null()
            {
                // Act
                var exception = Assert.Throws<ArgumentException>(() => errorMessageMapper.Map(null));

                // Assert
                exception.Message.ShouldEqual("The error message object property expression cannot be null");
            }

            [Test]
            public void Should_Add_Error_Message_Map_To_Private_Collection()
            {
                // Arrange
                Expression<Func<ErrorMessage, object>> errorMapPropertyExpression = x => x.Message;
                
                // Act
                var returnedInstance = errorMessageMapper.Map(errorMapPropertyExpression);

                // Assert
                var errorMaps = ((ErrorMessageMapperBase<ValidationErrors, ErrorMessage>)returnedInstance).ErrorMaps;
                errorMaps.ShouldNotBeNull();
                errorMaps.Count.ShouldEqual(1);
                errorMaps[0].Property.ShouldEqual(errorMapPropertyExpression);
            }

            [Test]
            public void Should_Add_Multiple_Error_Message_Maps_To_Private_Collection()
            {
                // Arrange
                Expression<Func<ErrorMessage, object>> errorMapPropertyExpression = x => x.Message;
                Expression<Func<ErrorMessage, object>> errorMapPropertyExpression2 = x => x.ErrorNumber;

                // Act
                errorMessageMapper.Map(errorMapPropertyExpression);
                var returnedInstance = errorMessageMapper.Map(errorMapPropertyExpression2);

                // Assert
                var errorMaps = ((ErrorMessageMapperBase<ValidationErrors, ErrorMessage>)returnedInstance).ErrorMaps;
                errorMaps.ShouldNotBeNull();
                errorMaps.Count.ShouldEqual(2);
                errorMaps[0].Property.ShouldEqual(errorMapPropertyExpression);
                errorMaps[1].Property.ShouldEqual(errorMapPropertyExpression2);
            }
        }

        public class AddAction : ErrorMessageMapperTests
        {
            [Test]
            public void Should_Add_Action()
            {
                // Arrange
                Action<List<ValidationErrors>> action = e => { };

                // Act
                
                var returnedInstance = errorMessageMapper.AddAction(action);

                // Assert
                var actions = ((ErrorMessageMapperBase<ValidationErrors, ErrorMessage>) returnedInstance).Actions;
                actions.Count.ShouldEqual(1);
                actions[0].ShouldEqual(action);
            }

            [Test]
            public void Should_Add_Multiple_Actions()
            {
                // Arrange
                Action<List<ValidationErrors>> action1 = e => { };
                Action<List<ValidationErrors>> action2 = e => { };

                // Act

                errorMessageMapper.AddAction(action1);
                var returnedInstance = errorMessageMapper.AddAction(action2);

                // Assert
                var actions = ((ErrorMessageMapperBase<ValidationErrors, ErrorMessage>)returnedInstance).Actions;
                actions.Count.ShouldEqual(2);
                actions[0].ShouldEqual(action1);
                actions[1].ShouldEqual(action2);
            }
        }

        public class ToErrorMessage : ErrorMessageMapperTests
        {
            [Test]
            public void Should_Set_Last_Error_Message_Map_ToErrorMessage_To_True()
            {
                // Arrange
                Expression<Func<ErrorMessage, object>> errorMapPropertyExpression = x => x.Message;
                var returnedInstance = errorMessageMapper.Map(errorMapPropertyExpression);

                // Act
                returnedInstance.ToErrorMessage();

                // Assert
                var errorMaps = ((ErrorMessageMapperBase<ValidationErrors, ErrorMessage>)returnedInstance).ErrorMaps;
                errorMaps.ShouldNotBeNull();
                errorMaps.Count.ShouldEqual(1);
                errorMaps[0].ToErrorMessage.ShouldBeTrue();
            }
        }

        public class ResolveUsing : ErrorMessageMapperTests
        {
            [Test]
            public void Should_Set_RsolveUsing_To_Passed_In_Action()
            {
                // Arrange
                var returnedInstance = errorMessageMapper.Map(x => x.Message);
                Func<string, object> resolveUsingAction = x => null;

                // Act

                returnedInstance.ResolveUsing(resolveUsingAction);

                // Assert
                var errorMaps = ((ErrorMessageMapperBase<ValidationErrors, ErrorMessage>)returnedInstance).ErrorMaps;
                errorMaps.ShouldNotBeNull();
                errorMaps.Count.ShouldEqual(1);
                errorMaps[0].ToErrorMessage.ShouldBeFalse();
                errorMaps[0].Resolver.ShouldEqual(resolveUsingAction);
            }
        }

        public class CreateValidationErrorResponse : ErrorMessageMapperTests
        {
            [Test]
            public void Should_Return_Valid_Validation_Error_For_ModelState_Item()
            {
                // Arrange
                var value = new ModelState();
                value.Errors.Add(new ModelError("Name is required"));
                var modelState = new KeyValuePair<string, ModelState>("Name", value);
                errorMessageMapper.ErrorMessagesMap = x => x.ErrorMessages;
                errorMessageMapper.ErrorMaps.Add(new ErrorMap<ErrorMessage>
                {
                    Property = x => x.Message,
                    ToErrorMessage = true
                });

                // Act
                var validationItem = errorMessageMapper.CreateValidationErrorModel(new RequestValidatorOptions(), modelState);

                // Assert
                validationItem.Property.ShouldEqual("Name");
                validationItem.ErrorMessages.Count.ShouldEqual(1);
                validationItem.ErrorMessages[0].Message.ShouldEqual("Name is required");
            }
        }
    }
}