﻿using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Hosting;
using System.Web.Http.Routing;
using Embarr.WebAPI.Validation.Attributes;
using NUnit.Framework;
using Should;

namespace Embarr.WebAPI.Validation.UnitTests.Attributes
{
    [TestFixture]
    public class DefaultValidateModelAttributeTests
    {
        private DefaultValidateModelAttribute defaultValidateModelAttribute;

        [SetUp]
        public void SetUp()
        {
            defaultValidateModelAttribute = new DefaultValidateModelAttribute();
        }

        public class OnActionExecuting : DefaultValidateModelAttributeTests
        {
            [Test]
            public void Should_Create_BadRequest_Response_With_ModelState_If_ModelState_Is_Invalid()
            {
                // Arrange
                var controllerContext = new HttpControllerContext(new HttpConfiguration(), new HttpRouteData(new HttpRoute()), new HttpRequestMessage());
                var httpActionContext = new HttpActionContext(controllerContext, new ReflectedHttpActionDescriptor());

                controllerContext.Request.Properties.Add(HttpPropertyKeys.HttpConfigurationKey, new HttpConfiguration());

                httpActionContext.ModelState.AddModelError("name", "error");

                // Act
                defaultValidateModelAttribute.OnActionExecuting(httpActionContext);

                // Assert
                var response = httpActionContext.Response;
                response.StatusCode.ShouldEqual(HttpStatusCode.BadRequest);
                var body = response.Content.ReadAsStringAsync().Result;
                body.ShouldContain("name");
                body.ShouldContain("error");
            }

            [Test]
            public void Should_Not_Create_BadRequest_Response_If_ModelState_Is_Valid()
            {
                // Arrange
                var controllerContext = new HttpControllerContext(new HttpConfiguration(), new HttpRouteData(new HttpRoute()), new HttpRequestMessage());
                var httpActionContext = new HttpActionContext(controllerContext, new ReflectedHttpActionDescriptor());

                controllerContext.Request.Properties.Add(HttpPropertyKeys.HttpConfigurationKey, new HttpConfiguration());

                // Act
                defaultValidateModelAttribute.OnActionExecuting(httpActionContext);

                // Assert
                httpActionContext.Response.ShouldBeNull();
            }
        }

        public class OnActionExecutingAsync : DefaultValidateModelAttributeTests
        {
            [Test]
            public async Task Should_Create_BadRequest_Response_With_ModelState_If_ModelState_Is_Invalid()
            {
                // Arrange
                var controllerContext = new HttpControllerContext(new HttpConfiguration(), new HttpRouteData(new HttpRoute()), new HttpRequestMessage());
                var httpActionContext = new HttpActionContext(controllerContext, new ReflectedHttpActionDescriptor());
                var tokenSource = new CancellationTokenSource();
                
                controllerContext.Request.Properties.Add(HttpPropertyKeys.HttpConfigurationKey, new HttpConfiguration());

                httpActionContext.ModelState.AddModelError("name", "error");

                // Act
                await defaultValidateModelAttribute.OnActionExecutingAsync(httpActionContext, tokenSource.Token);

                // Assert
                var response = httpActionContext.Response;
                response.StatusCode.ShouldEqual(HttpStatusCode.BadRequest);
                var body = response.Content.ReadAsStringAsync().Result;
                body.ShouldContain("name");
                body.ShouldContain("error");
            }

            [Test]
            public async Task Should_Not_Create_BadRequest_Response_If_ModelState_Is_Valid()
            {
                // Arrange
                var controllerContext = new HttpControllerContext(new HttpConfiguration(), new HttpRouteData(new HttpRoute()), new HttpRequestMessage());
                var httpActionContext = new HttpActionContext(controllerContext, new ReflectedHttpActionDescriptor());

                controllerContext.Request.Properties.Add(HttpPropertyKeys.HttpConfigurationKey, new HttpConfiguration());
                var tokenSource = new CancellationTokenSource();

                // Act
                await defaultValidateModelAttribute.OnActionExecutingAsync(httpActionContext, tokenSource.Token);

                // Assert
                httpActionContext.Response.ShouldBeNull();
            }
        }
    }
}