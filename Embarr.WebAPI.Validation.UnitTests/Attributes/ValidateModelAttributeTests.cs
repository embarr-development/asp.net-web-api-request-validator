﻿using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Hosting;
using System.Web.Http.Routing;
using Embarr.WebAPI.Validation.Attributes;
using Embarr.WebAPI.Validation.UnitTests.TestModels;
using NUnit.Framework;
using Should;

namespace Embarr.WebAPI.Validation.UnitTests.Attributes
{
    [TestFixture]
    public class ValidateModelAttributeTests
    {
        private ValidateModelAttribute validateModelAttribute;

        public class OnActionExecuting : ValidateModelAttributeTests
        {
            [Test]
            public void Should_Create_BadRequest_Response_With_Content_Return_By_Func()
            {
                // Arrange
                var errorResponse = new object[] { "This is an error" };
                validateModelAttribute = new ValidateModelAttribute((m, r) => errorResponse, typeof(ValidationErrors), new RequestValidatorOptions());
                var controllerContext = new HttpControllerContext(new HttpConfiguration(), new HttpRouteData(new HttpRoute()), new HttpRequestMessage());
                var httpActionContext = new HttpActionContext(controllerContext, new ReflectedHttpActionDescriptor());

                controllerContext.Request.Properties.Add(HttpPropertyKeys.HttpConfigurationKey, new HttpConfiguration());

                httpActionContext.ModelState.AddModelError("name", "error");

                // Act
                validateModelAttribute.OnActionExecuting(httpActionContext);

                // Assert
                var response = httpActionContext.Response;
                response.StatusCode.ShouldEqual(HttpStatusCode.BadRequest);
                var body = response.Content.ReadAsStringAsync().Result;
                body.ShouldContain("This is an error");
            }

            [Test]
            public void Should_Not_Create_BadRequest_Response_If_ModelState_Is_Valid()
            {
                // Arrange
                validateModelAttribute = new ValidateModelAttribute((m, r) => new object[]{}, typeof(ValidationErrors), new RequestValidatorOptions());
                var controllerContext = new HttpControllerContext(new HttpConfiguration(), new HttpRouteData(new HttpRoute()), new HttpRequestMessage());
                var httpActionContext = new HttpActionContext(controllerContext, new ReflectedHttpActionDescriptor());

                controllerContext.Request.Properties.Add(HttpPropertyKeys.HttpConfigurationKey, new HttpConfiguration());

                // Act
                validateModelAttribute.OnActionExecuting(httpActionContext);

                // Assert
                httpActionContext.Response.ShouldBeNull();
            }
        }

        public class OnActionExecutingAsync : ValidateModelAttributeTests
        {
            [Test]
            public async Task Should_Create_BadRequest_Response_With_Content_Return_By_Func()
            {
                // Arrange
                var errorResponse = new object[] {"This is an error"};
                validateModelAttribute = new ValidateModelAttribute((m, r) => errorResponse, typeof(ValidationErrors), new RequestValidatorOptions());
                var controllerContext = new HttpControllerContext(new HttpConfiguration(), new HttpRouteData(new HttpRoute()), new HttpRequestMessage());
                var httpActionContext = new HttpActionContext(controllerContext, new ReflectedHttpActionDescriptor());
                var tokenSource = new CancellationTokenSource();

                controllerContext.Request.Properties.Add(HttpPropertyKeys.HttpConfigurationKey, new HttpConfiguration());

                httpActionContext.ModelState.AddModelError("name", "error");

                // Act
                await validateModelAttribute.OnActionExecutingAsync(httpActionContext, tokenSource.Token);

                // Assert
                var response = httpActionContext.Response;
                response.StatusCode.ShouldEqual(HttpStatusCode.BadRequest);
                var body = response.Content.ReadAsStringAsync().Result;
                body.ShouldContain("This is an error");
            }

            [Test]
            public async Task Should_Not_Create_BadRequest_Response_If_ModelState_Is_Valid()
            {
                // Arrange
                validateModelAttribute = new ValidateModelAttribute((m, r) => new object[] { }, typeof(ValidationErrors), new RequestValidatorOptions());
                var controllerContext = new HttpControllerContext(new HttpConfiguration(), new HttpRouteData(new HttpRoute()), new HttpRequestMessage());
                var httpActionContext = new HttpActionContext(controllerContext, new ReflectedHttpActionDescriptor());

                controllerContext.Request.Properties.Add(HttpPropertyKeys.HttpConfigurationKey, new HttpConfiguration());
                var tokenSource = new CancellationTokenSource();

                // Act
                await validateModelAttribute.OnActionExecutingAsync(httpActionContext, tokenSource.Token);

                // Assert
                httpActionContext.Response.ShouldBeNull();
            }
        }
    }
}