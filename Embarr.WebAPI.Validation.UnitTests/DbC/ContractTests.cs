﻿using System;
using Amido.NAuto;
using Embarr.WebAPI.Validation.DbC;
using NUnit.Framework;
using Should;

namespace Embarr.WebAPI.Validation.UnitTests.DbC
{
    [TestFixture]
    public class ContractTests
    {
        [Test]
        public void Should_Throw_Exception_With_Specified_Message_If_Condition_Is_False()
        {
            // Arrange
            var message = NAuto.GetRandomString(20);

            // Act
            var exception = Assert.Throws<ArgumentException>(() => Contract.Requires(false, message));

            // Assert
            exception.Message.ShouldEqual(message);
        }

        [Test]
        public void Should_Not_Throw_Exception_If_Condition_Is_True()
        {
            // Arrange
            var message = NAuto.GetRandomString(20);

            // Assert
            Assert.DoesNotThrow(() => Contract.Requires(true, message));
        }
    }
}