﻿using System;
using System.Linq;
using System.Web.Http;
using Embarr.WebAPI.Validation.Attributes;
using Embarr.WebAPI.Validation.Mappers;
using Embarr.WebAPI.Validation.UnitTests.TestModels;
using NUnit.Framework;
using Should;

namespace Embarr.WebAPI.Validation.UnitTests
{
    [TestFixture]
    public class RequestValidatorTests
    {
        public class UseDefaultModelStateFormat : RequestValidatorTests
        {
            [Test]
            public void Should_Throw_Exception_If_HttpConfiguration_Is_Null()
            {
                // Act
                var exception = Assert.Throws<ArgumentException>(() => RequestValidator.UseDefaultModelStateFormat(null));

                // Assert
                exception.Message.ShouldEqual("The httpConfiguration cannot be null");
            } 

            [Test]
            public void Should_Add_Default_ModelState_Attribute_To_Filters()
            {
                // Arrange
                var httpConfiguration = new HttpConfiguration();
                
                // Act
                RequestValidator.UseDefaultModelStateFormat(httpConfiguration);

                // Assert
                httpConfiguration.Filters.Any(x => x.Instance.GetType() == typeof(DefaultValidateModelAttribute)).ShouldBeTrue();
            }   
        }

        public class Create : RequestValidatorTests
        {
            [Test]
            public void Should_Return_Property_Mapper_Of_Correct_Generic_Type()
            {
                // Act
                var propertyMapper = RequestValidator.Create<ValidationErrors>();

                // Assert
                propertyMapper.ShouldNotBeNull();
                propertyMapper.ShouldBeType<PropertyMapper<ValidationErrors>>();
            }
        }
    }
}