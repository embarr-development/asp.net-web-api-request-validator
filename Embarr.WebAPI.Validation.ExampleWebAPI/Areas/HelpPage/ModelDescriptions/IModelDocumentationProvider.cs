using System;
using System.Reflection;

namespace Embarr.WebAPI.Validation.ExampleWebAPI.Areas.HelpPage.ModelDescriptions
{
    public interface IModelDocumentationProvider
    {
        string GetDocumentation(MemberInfo member);

        string GetDocumentation(Type type);
    }
}