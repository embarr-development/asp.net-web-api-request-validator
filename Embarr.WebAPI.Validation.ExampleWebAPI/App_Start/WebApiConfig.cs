﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using Microsoft.Owin.Security.OAuth;
using Newtonsoft.Json.Serialization;

namespace Embarr.WebAPI.Validation.ExampleWebAPI
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services
            // Configure Web API to use only bearer token authentication.
            config.SuppressDefaultHostAuthentication();
            config.Filters.Add(new HostAuthenticationFilter(OAuthDefaults.AuthenticationType));

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            RequestValidator.Create<ValidationError>()
                .MapPropertyName(x => x.Property)
                .MapPropertyErrorMessagesCollection(x => x.Errors)
                .Map(x => x.Message).ToErrorMessage()
                .Init(config);
        }
    }

    public class ValidationError
    {
        public string Property { get; set; }
        public List<Error> Errors { get; set; }

        public ValidationError()
        {
            Errors = new List<Error>();
        }
    }

    public class Error
    {
        public string Message { get; set; }
    }
}
