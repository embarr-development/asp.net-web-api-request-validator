﻿using System.Web;
using System.Web.Mvc;

namespace Embarr.WebAPI.Validation.ExampleWebAPI
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
