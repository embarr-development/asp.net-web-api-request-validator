﻿using System.ComponentModel.DataAnnotations;

namespace Embarr.WebAPI.Validation.ExampleWebAPI.Models
{
    public class ValidationTestModel
    {
        [Required]
        public string Name { get; set; }

        [Required]
        [Range(18, 65)]
        public string Age { get; set; }
    }
}