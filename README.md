# ASP.Net Web API Validator #

Utilize DataAnnotations on your ASP.Net Web API models and return your own complex validation models, flattened models or default model state models.

For detailed documentation include features not detailed here please visit the [Embarr Development Labs](http://embarr-development.co.uk/labs/) page.

There is a fully working sample ASP.Net Web API project called **Embarr.WebAPI.Validation.ExampleWebAPI** in source.

## Examples ##

### Full Custom Validation Model ###

Lets say you have the following custom classes which you want for your validation response:

    public class ValidationError
    {
        public string Property { get; set; }
        public List<ErrorMessage> ErrorMessages { get; set; }

        public ValidationError()
        {
            ErrorMessages = new List<ErrorMessage>();
        }
    }

and

    public class ErrorMessage
    {
        public int ErrorNumber { get; set; }
        public string Message { get; set; }
    }

You can map your custom classes in the request Validator and have the data annotations messages displayed.

Put the following in your Web API config or app start.

    RequestValidator.Create<ValidationError>()
       .MapPropertyName(x => x.Property)
       .MapPropertyErrorMessagesCollection(x => x.ErrorMessages)
       .Map(x => x.Message).ToErrorMessage()
       .Init(httpConfiguration);

## Flattened Error Messages ##
Use this if you want simple key => error style messages.
The library will simply take the first validation message attached to a property and return that.

Lets say you have the following custom class which you want for your validation response:

    public class ValidationError
    {
        public string Property { get; set; }
        public string Message{ get; set; }
    }

Put the following in your Web API config or app start.

    RequestValidator.Create<ValidationError>()
        .MapPropertyName(x => x.Property)
        .FlattenErrorMessages()
        .Map(x => x.Message).ToErrorMessage()
        .Init(httpConfiguration);

## Use Default ModelState Object ##
To use the built in ModelState model rather than map your own, just add the following to your  Web API config or app start:

    RequestValidator.UseDefaultModelStateFormat(httpConfiguration);

You may find it you're using json that the default ModelState object returned lots of badly named property names for example <Exception>k__BackingField...
To get around this just add the following code in the same method:

    var serializerSettings = httpConfiguration.Formatters.JsonFormatter.SerializerSettings;
    var contractResolver = (DefaultContractResolver)serializerSettings.ContractResolver;
    contractResolver.IgnoreSerializableAttribute = true;

## What Will This Look Like ##
Lets say you're using the following model:

    public class MyModel
    {
        [Required]
        public string Name { get; set; }

        [Range(10, 20)]
        public int Age { get; set; }
    }

If both properties failed validation and entered ModelState and you were using the mapping and validation classes from 'Full Custom Validation Model' above, you would get the following json or xml: 

#### JSON Output: ####

    [ 
      { 
         "Property" : "Name",
         "ErrorMessages" : [ { "Message" : "The Name field is required." } ]       
      },
      { 
         "Property" : "Age",
         "ErrorMessages" : [ { "Message" : "The field Age must be between 10 and 20." } ]       
      }
    ]

#### XML Output ####

    <ArrayOfanyType xmlns:i="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://schemas.microsoft.com/2003/10/Serialization/Arrays">
        <anyType xmlns:d2p1="http://schemas.datacontract.org/2004/07/Embarr.WebAPI.Validation.AcceptanceTests.WebApi.Models" i:type="d2p1:ValidationError">
            <d2p1:Property>Name</d2p1:Property>
            <d2p1:ErrorMessages>
                <d2p1:ErrorMessage>
                    <d2p1:Message>The Name field is required.</d2p1:Message>
                </d2p1:ErrorMessage>
            </d2p1:ErrorMessages>
        </anyType>
        <anyType xmlns:d2p1="http://schemas.datacontract.org/2004/07/Embarr.WebAPI.Validation.AcceptanceTests.WebApi.Models" i:type="d2p1:ValidationError">
            <d2p1:Property>Age</d2p1:Property>
            <d2p1:ErrorMessages>
                <d2p1:ErrorMessage>
                    <d2p1:Message>The field Age must be between 10 and 20.</d2p1:Message>
                </d2p1:ErrorMessage>
            </d2p1:ErrorMessages>
        </anyType>
    </ArrayOfanyType>