using System;

namespace Embarr.WebAPI.Validation.Contracts
{
    public interface IDecideHowToMapFlattenedErrorMessages<TValidationErrorModel>
    {
        IMapFlattenedErrorMessagesAndInitialize<TValidationErrorModel> ToErrorMessage();
        IMapFlattenedErrorMessagesAndInitialize<TValidationErrorModel> ResolveUsing(Func<string, object> resolver);
    }
}