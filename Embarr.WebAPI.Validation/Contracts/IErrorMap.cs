using System;

namespace Embarr.WebAPI.Validation.Contracts
{
    public interface IErrorMap
    {
        bool ToErrorMessage { get; set; }
        Func<string, object> Resolver { get; set; }
    }
}