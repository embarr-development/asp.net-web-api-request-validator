using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Web.Http;

namespace Embarr.WebAPI.Validation.Contracts
{
    public interface IMapErrorMessagesAndInitialize<TValidationErrorModel, TErrorMessage>
    {
        /// <summary>
        /// Creates a pointer to a property on the error model.
        /// </summary>
        /// <param name="property">The property.</param>
        /// <returns></returns>
        IDecideHowToMapErrorMessages<TValidationErrorModel, TErrorMessage> Map(Expression<Func<TErrorMessage, object>> property);

        /// <summary>
        /// Adds the action which is called with the populated error model.
        /// </summary>
        /// <param name="action">The action.</param>
        /// <returns></returns>
        IMapErrorMessagesAndInitialize<TValidationErrorModel, TErrorMessage> AddAction(Action<List<TValidationErrorModel>> action);

        /// <summary>
        /// Initializes request validator.
        /// </summary>
        /// <param name="httpConfiguration">The HTTP configuration.</param>
        /// <param name="requestValidatorOptions">The request validator options.</param>
        void Init(HttpConfiguration httpConfiguration, RequestValidatorOptions requestValidatorOptions = null);
    }
}