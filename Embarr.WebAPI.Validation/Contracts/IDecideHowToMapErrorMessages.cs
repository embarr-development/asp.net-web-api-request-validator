using System;

namespace Embarr.WebAPI.Validation.Contracts
{
    public interface IDecideHowToMapErrorMessages<TValidationErrorModel, TErrorMessage>
    {
        IMapErrorMessagesAndInitialize<TValidationErrorModel, TErrorMessage> ToErrorMessage();
        IMapErrorMessagesAndInitialize<TValidationErrorModel, TErrorMessage> ResolveUsing(Func<string, object> modelStateErrorMessage);
    }
}