﻿using System;
using System.Collections.Generic;

namespace Embarr.WebAPI.Validation.Contracts
{
    public interface IMapPropertyErrorMessages<TValidationErrorModel>
    {
        /// <summary>
        /// Creates a pointer to a collection where the errored property error messages will be stored.
        /// </summary>
        /// <typeparam name="TErrorMessage">The type of the error message.</typeparam>
        /// <param name="errorMessages">The error messages.</param>
        /// <returns></returns>
        IMapErrorMessagesAndInitialize<TValidationErrorModel, TErrorMessage> MapPropertyErrorMessagesCollection<TErrorMessage>(Func<TValidationErrorModel, ICollection<TErrorMessage>> errorMessages) where TErrorMessage : new();

        /// <summary>
        /// Use this method when the errored property and error message are within the same class at the same level.
        /// NOTE: Only the first error message for an errored property is used if you use this method. 
        /// </summary>
        /// <returns></returns>
        IMapFlattenedErrorMessagesAndInitialize<TValidationErrorModel> FlattenErrorMessages();
    }
}