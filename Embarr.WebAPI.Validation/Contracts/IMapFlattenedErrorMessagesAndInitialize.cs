using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Web.Http;

namespace Embarr.WebAPI.Validation.Contracts
{
    public interface IMapFlattenedErrorMessagesAndInitialize<TValidationErrorModel>
    {
        /// <summary>
        /// Creates a pointer to the property where the error message is to be stored.
        /// </summary>
        /// <param name="errorMessageMap">The error message map.</param>
        /// <returns></returns>
        IDecideHowToMapFlattenedErrorMessages<TValidationErrorModel> Map(
            Expression<Func<TValidationErrorModel, object>> errorMessageMap);


        /// <summary>
        /// Adds the action which is called with the populated error model.
        /// </summary>
        /// <param name="action">The action.</param>
        /// <returns></returns>
        IMapFlattenedErrorMessagesAndInitialize<TValidationErrorModel> AddAction(Action<List<TValidationErrorModel>> action);


        /// <summary>
        /// Initializes request validator.
        /// </summary>
        /// <param name="httpConfiguration">The HTTP configuration.</param>
        /// <param name="requestValidatorOptions">The request validator options.</param>
        void Init(HttpConfiguration httpConfiguration, RequestValidatorOptions requestValidatorOptions = null);
        
    }
}