﻿using System;
using System.Linq.Expressions;

namespace Embarr.WebAPI.Validation.Contracts
{
    public interface IMapPropertyName<TValidationErrorModel>
    {
        /// <summary>
        /// Creates a pointer to the property where the name of the errored property will be stored.
        /// </summary>
        /// <param name="property">The property.</param>
        /// <returns></returns>
        IMapPropertyErrorMessages<TValidationErrorModel> MapPropertyName(Expression<Func<TValidationErrorModel, string>> property);
    }
}