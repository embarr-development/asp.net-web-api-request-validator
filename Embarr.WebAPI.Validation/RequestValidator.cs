﻿using System.Web.Http;
using Embarr.WebAPI.Validation.Attributes;
using Embarr.WebAPI.Validation.Contracts;
using Embarr.WebAPI.Validation.DbC;
using Embarr.WebAPI.Validation.Mappers;

namespace Embarr.WebAPI.Validation
{
    /// <summary>
    /// Fluent API for generating custom model validation formats when using DataAnnotation on Web API models.
    /// </summary>
    public static class RequestValidator
    {
        /// <summary>
        /// Uses the default model state format.
        /// </summary>
        /// <param name="httpConfiguration">The HTTP configuration.</param>
        public static void UseDefaultModelStateFormat(HttpConfiguration httpConfiguration)
        {
            Contract.Requires(httpConfiguration != null, "The httpConfiguration cannot be null");

            httpConfiguration.Filters.Add(new DefaultValidateModelAttribute());
        }

        /// <summary>
        /// Creates a model validator and starts of the fluent API.
        /// </summary>
        /// <typeparam name="TValidationErrorModel">The type of the validation error model.</typeparam>
        /// <returns></returns>
        public static IMapPropertyName<TValidationErrorModel> Create<TValidationErrorModel>()
            where TValidationErrorModel : class
        {
            return new PropertyMapper<TValidationErrorModel>();
        }
    }
}