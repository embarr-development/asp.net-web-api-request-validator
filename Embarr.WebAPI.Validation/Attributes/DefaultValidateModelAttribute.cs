﻿using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace Embarr.WebAPI.Validation.Attributes
{
    public class DefaultValidateModelAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            if (actionContext.ModelState.IsValid)
            {
                base.OnActionExecuting(actionContext);
            }
            else
            {
                actionContext.Response = CreateValidationErrorResponse(actionContext);
            }
        }

        public override Task OnActionExecutingAsync(HttpActionContext actionContext, CancellationToken cancellationToken)
        {
            return actionContext.ModelState.IsValid 
                ? base.OnActionExecutingAsync(actionContext, cancellationToken) 
                : Task.FromResult(CreateValidationErrorResponse(actionContext));
        }

        private HttpResponseMessage CreateValidationErrorResponse(HttpActionContext actionContext)
        {
            var response = actionContext.Request.CreateResponse(HttpStatusCode.BadRequest, actionContext.ModelState);
            actionContext.Response = response;
            return response;
        }
    }
}
