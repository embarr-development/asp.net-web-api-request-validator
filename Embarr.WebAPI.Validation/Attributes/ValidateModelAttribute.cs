﻿using System;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using System.Web.Http.ModelBinding;

namespace Embarr.WebAPI.Validation.Attributes
{
    public class ValidateModelAttribute : ActionFilterAttribute
    {
        private readonly Func<ModelStateDictionary, RequestValidatorOptions, object[]> action;
        private readonly Type type;
        private readonly RequestValidatorOptions requestValidatorOptions;

        public ValidateModelAttribute(Func<ModelStateDictionary, RequestValidatorOptions, object[]> action, Type type, RequestValidatorOptions requestValidatorOptions)
        {
            this.action = action;
            this.type = type;
            this.requestValidatorOptions = requestValidatorOptions;
        }

        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            if (actionContext.ModelState.IsValid)
            {
                base.OnActionExecuting(actionContext);
            }
            else
            {
                actionContext.Response = CreateValidationErrorResponse(actionContext);
            }
        }

        public override Task OnActionExecutingAsync(HttpActionContext actionContext, CancellationToken cancellationToken)
        {
            var response =  actionContext.ModelState.IsValid
                ? base.OnActionExecutingAsync(actionContext, cancellationToken)
                : Task.FromResult(CreateValidationErrorResponse(actionContext));
            
            return response;
        }

        private HttpResponseMessage CreateValidationErrorResponse(HttpActionContext actionContext)
        {
            var obj = action(actionContext.ModelState, requestValidatorOptions);
            var response = actionContext.Request.CreateResponse(requestValidatorOptions.HttpStatusCode, obj);
            actionContext.Response = response;
            return response;
        }
    }
}