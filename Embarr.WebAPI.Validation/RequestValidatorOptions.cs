﻿using System.Net;

namespace Embarr.WebAPI.Validation
{
    /// <summary>
    /// Sets options for the request validator.
    /// </summary>
    public class RequestValidatorOptions
    {
        /// <summary>
        /// Gets or sets the HTTP status code.
        /// </summary>
        /// <value>
        /// The HTTP status code.
        /// </value>
        public HttpStatusCode HttpStatusCode { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether to show the model name.
        /// </summary>
        /// <value>
        ///   <c>true</c> if [show model name]; otherwise, <c>false</c>.
        /// </value>
        public bool ShowModelName { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="RequestValidatorOptions"/> class.
        /// </summary>
        public RequestValidatorOptions()
        {
            HttpStatusCode = HttpStatusCode.BadRequest;
        }
    }
}