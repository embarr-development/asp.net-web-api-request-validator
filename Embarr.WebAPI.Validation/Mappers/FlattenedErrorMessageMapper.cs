﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web.Http.ModelBinding;
using Embarr.WebAPI.Validation.Contracts;
using Embarr.WebAPI.Validation.DbC;

namespace Embarr.WebAPI.Validation.Mappers
{
    public class FlattenedErrorMessageMapper<TValidationErrorModel> :
        ErrorMessageMapperBase<TValidationErrorModel, TValidationErrorModel>,
        IMapFlattenedErrorMessagesAndInitialize<TValidationErrorModel>, 
        IDecideHowToMapFlattenedErrorMessages<TValidationErrorModel>
        where TValidationErrorModel : class
    {
        public FlattenedErrorMessageMapper(Expression<Func<TValidationErrorModel, string>> erroredPropertyMap)
        {
            Contract.Requires(erroredPropertyMap != null, "The errored property expression cannot be null");
            ErroredPropertyMap = erroredPropertyMap;
        }
        
        public IDecideHowToMapFlattenedErrorMessages<TValidationErrorModel> Map(Expression<Func<TValidationErrorModel, object>> errorMessageMap)
        {
            SaveMap(errorMessageMap);
            return this;
        }

        public IMapFlattenedErrorMessagesAndInitialize<TValidationErrorModel> AddAction(Action<List<TValidationErrorModel>> action)
        {
            SaveAction(action);
            return this;
        }

        public IMapFlattenedErrorMessagesAndInitialize<TValidationErrorModel> ToErrorMessage()
        {
            SetToErrorMessage();
            return this;
        }

        public IMapFlattenedErrorMessagesAndInitialize<TValidationErrorModel> ResolveUsing(Func<string, object> resolver)
        {
            SaveResolver(resolver);
            return this;
        }

        internal override TValidationErrorModel CreateValidationErrorModel(RequestValidatorOptions requestValidatorOptions,
            KeyValuePair<string, ModelState> item)
        {
            var validationErrorModel = CreateModelAndSetErroredPropertyName(requestValidatorOptions, item);

            SetErrorProperty(validationErrorModel, item.Value.Errors.First());
            return validationErrorModel;
        }
    }
}