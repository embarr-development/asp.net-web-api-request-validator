using System;
using System.Linq.Expressions;
using Embarr.WebAPI.Validation.Contracts;

namespace Embarr.WebAPI.Validation.Mappers
{
    public class ErrorMap<TErrorMessage> : IErrorMap
    {
        public Expression<Func<TErrorMessage, object>> Property { get; set; }
        public bool ToErrorMessage { get; set; }
        public Func<string, object> Resolver { get; set; }
    }
}