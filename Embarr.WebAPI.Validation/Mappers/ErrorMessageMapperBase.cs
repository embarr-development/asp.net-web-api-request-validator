﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Runtime.Serialization;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using Embarr.WebAPI.Validation.Attributes;
using Embarr.WebAPI.Validation.Cache;
using Embarr.WebAPI.Validation.DbC;

namespace Embarr.WebAPI.Validation.Mappers
{
    public abstract class ErrorMessageMapperBase<TValidationErrorModel, TErrorMessagesModel>
        where TValidationErrorModel : class
    {
        internal Expression<Func<TValidationErrorModel, string>> ErroredPropertyMap { get; set; }
        internal List<Action<List<TValidationErrorModel>>> Actions { get; set; }
        internal List<ErrorMap<TErrorMessagesModel>> ErrorMaps { get; private set; }

        protected ErrorMessageMapperBase()
        {
            Actions = new List<Action<List<TValidationErrorModel>>>();
            ErrorMaps = new List<ErrorMap<TErrorMessagesModel>>();
        }

        public void Init(HttpConfiguration httpConfiguration, RequestValidatorOptions requestValidatorOptions = null)
        {
            if (requestValidatorOptions == null)
            {
                requestValidatorOptions = Activator.CreateInstance<RequestValidatorOptions>();
            }

            httpConfiguration.Filters.Add(new ValidateModelAttribute(CreateValidationErrorResponse, typeof(List<TValidationErrorModel>), requestValidatorOptions));

            httpConfiguration.Formatters.XmlFormatter.SetSerializer<object[]>(
                new DataContractSerializer(typeof(object[]), new[] { typeof(TValidationErrorModel) }));
        }

        protected void SaveMap(Expression<Func<TErrorMessagesModel, object>> property)
        {
            Contract.Requires(property != null, "The error message object property expression cannot be null");
            ErrorMaps.Add(new ErrorMap<TErrorMessagesModel>
            {
                Property = property
            });
        }

        protected void SaveAction(Action<List<TValidationErrorModel>> action)
        {
            Actions.Add(action);
        }

        protected void SetToErrorMessage()
        {
            ErrorMaps.Last().ToErrorMessage = true;
        }

        protected void SaveResolver(Func<string, object> resolver)
        {
            ErrorMaps.Last().Resolver = resolver;
        }

        protected TValidationErrorModel[] CreateValidationErrorResponse(ModelStateDictionary modelStateDictionary, RequestValidatorOptions requestValidatorOptions)
        {
            var validationErrorModels = new List<TValidationErrorModel>();

            foreach (var item in modelStateDictionary)
            {
                if (item.Value.Errors.Count == 0)
                {
                    continue;
                }

                var validationErrorModel = CreateValidationErrorModel(requestValidatorOptions, item);

                validationErrorModels.Add(validationErrorModel);
            }

            RunActions(validationErrorModels);

            return validationErrorModels.ToArray();
        }

        internal abstract TValidationErrorModel CreateValidationErrorModel(
            RequestValidatorOptions requestValidatorOptions,
            KeyValuePair<string, ModelState> item);

        protected void SetErrorProperty(TErrorMessagesModel errorModel, ModelError error)
        {
            foreach (var errorMap in ErrorMaps)
            {
                Func<Expression, PropertyInfo> getProperty = GetPropertyInfo;
                var getPropertyMemoizer = getProperty.Memoize();
                var propertyInfo = getPropertyMemoizer(errorMap.Property.Body);

                if (errorMap.ToErrorMessage)
                {
                    propertyInfo.SetValue(errorModel, error.ErrorMessage);
                }
                else
                {
                    propertyInfo.SetValue(errorModel, errorMap.Resolver(error.ErrorMessage));
                }
            }
        }

        protected TValidationErrorModel CreateModelAndSetErroredPropertyName(RequestValidatorOptions requestValidatorOptions, KeyValuePair<string, ModelState> item)
        {
            var validationErrorModel = Activator.CreateInstance<TValidationErrorModel>();
            Func<Expression, PropertyInfo> getProperty = GetPropertyInfo;
            var getPropertyMemoizer = getProperty.Memoize();
            var property = getPropertyMemoizer(ErroredPropertyMap.Body as MemberExpression);
            property.SetValue(validationErrorModel, CleanKey(item.Key, requestValidatorOptions));
            return validationErrorModel;
        }

        private static string CleanKey(string key, RequestValidatorOptions requestValidatorOptions)
        {
            if (!requestValidatorOptions.ShowModelName)
            {
                var firstDot = key.IndexOf('.');
                return key.Substring(firstDot + 1, key.Length - firstDot - 1);
            }

            return key;
        }

        private static PropertyInfo GetPropertyInfo(Expression expression)
        {
            if (expression as MemberExpression != null)
            {
                return ((MemberExpression)expression).Member as PropertyInfo;
            }

            if (expression as UnaryExpression != null)
            {
                var unary = ((UnaryExpression)expression);
                return ((MemberExpression)unary.Operand).Member as PropertyInfo;
            }

            throw new NotSupportedException("Not supported at the moment");
        }

        private void RunActions(List<TValidationErrorModel> validationErrorModels)
        {
            foreach (var action in Actions)
            {
                action(validationErrorModels);
            }
        }
    }
}