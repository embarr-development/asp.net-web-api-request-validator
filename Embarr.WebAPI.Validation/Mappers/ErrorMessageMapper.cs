using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Web.Http.ModelBinding;
using Embarr.WebAPI.Validation.Contracts;
using Embarr.WebAPI.Validation.DbC;

namespace Embarr.WebAPI.Validation.Mappers
{
    public class ErrorMessageMapper<TValidationErrorModel, TErrorMessage> :
        ErrorMessageMapperBase<TValidationErrorModel, TErrorMessage>,
        IMapErrorMessagesAndInitialize<TValidationErrorModel, TErrorMessage>, 
        IDecideHowToMapErrorMessages<TValidationErrorModel, TErrorMessage>
        where TValidationErrorModel : class
    {

        internal Func<TValidationErrorModel, ICollection<TErrorMessage>> ErrorMessagesMap { get; set; }

        public ErrorMessageMapper(Expression<Func<TValidationErrorModel, string>> erroredPropertyMap, Func<TValidationErrorModel, ICollection<TErrorMessage>> errorMessagesMap)
        {
            Contract.Requires(erroredPropertyMap != null, "The errored property expression cannot be null");
            Contract.Requires(errorMessagesMap != null, "The error messages expression cannot be null");

            ErroredPropertyMap = erroredPropertyMap;
            ErrorMessagesMap = errorMessagesMap;
        }

        public IDecideHowToMapErrorMessages<TValidationErrorModel, TErrorMessage> Map(Expression<Func<TErrorMessage, object>> property)
        {
            SaveMap(property);
            return this;
        }

        public IMapErrorMessagesAndInitialize<TValidationErrorModel, TErrorMessage> AddAction(Action<List<TValidationErrorModel>> action)
        {
            SaveAction(action);
            return this;
        }

        public IMapErrorMessagesAndInitialize<TValidationErrorModel, TErrorMessage> ToErrorMessage()
        {
            SetToErrorMessage();
            return this;
        }

        public IMapErrorMessagesAndInitialize<TValidationErrorModel, TErrorMessage> ResolveUsing(Func<string, object> resolver)
        {
            SaveResolver(resolver);
            return this;
        }

        internal override TValidationErrorModel CreateValidationErrorModel(RequestValidatorOptions requestValidatorOptions,
            KeyValuePair<string, ModelState> item)
        {
            var validationErrorModel = CreateModelAndSetErroredPropertyName(requestValidatorOptions, item);

            foreach (var error in item.Value.Errors)
            {
                var errorModel = Activator.CreateInstance<TErrorMessage>();

                SetErrorProperty(errorModel, error);

                var errorCollection = ErrorMessagesMap.Invoke(validationErrorModel);
                errorCollection.Add(errorModel);
            }
            
            return validationErrorModel;
        }
    }
}