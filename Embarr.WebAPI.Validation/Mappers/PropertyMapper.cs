﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Embarr.WebAPI.Validation.Contracts;
using Embarr.WebAPI.Validation.DbC;

namespace Embarr.WebAPI.Validation.Mappers
{
    public class PropertyMapper<TValidationErrorModel> : IMapPropertyName<TValidationErrorModel>, IMapPropertyErrorMessages<TValidationErrorModel>
        where TValidationErrorModel : class
    {
        private Expression<Func<TValidationErrorModel, string>> errorProperty;

        /// <summary>
        /// Maps the name of the errored property.
        /// </summary>
        /// <param name="property">The errored property.</param>
        /// <returns></returns>
        public IMapPropertyErrorMessages<TValidationErrorModel> MapPropertyName(Expression<Func<TValidationErrorModel, string>> property)
        {
            Contract.Requires(property != null, "The property expression cannot be null");

            errorProperty = property;
            return this;
        }

        /// <summary>
        /// Maps the property error messages.
        /// </summary>
        /// <typeparam name="TErrorMessage">The type of the error message.</typeparam>
        /// <param name="errorMessages">The error messages.</param>
        /// <returns></returns>
        public IMapErrorMessagesAndInitialize<TValidationErrorModel, TErrorMessage> MapPropertyErrorMessagesCollection<TErrorMessage>(Func<TValidationErrorModel, ICollection<TErrorMessage>> errorMessages)
            where TErrorMessage : new()
        {
            Contract.Requires(errorMessages != null, "The errorMessages expression cannot be null");

            return new ErrorMessageMapper<TValidationErrorModel, TErrorMessage>(errorProperty, errorMessages);
        }

        /// <summary>
        /// Flattens the error model allowing the error message to be a property of the top level error object next to the property name.
        /// NOTE: This will take the 1st error messages attached to the errored property, other errors will not be returned.
        /// </summary>
        /// <returns></returns>
        public IMapFlattenedErrorMessagesAndInitialize<TValidationErrorModel> FlattenErrorMessages()
        {
            return new FlattenedErrorMessageMapper<TValidationErrorModel>(errorProperty);
        }
    }
}