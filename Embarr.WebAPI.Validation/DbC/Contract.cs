﻿using System;

namespace Embarr.WebAPI.Validation.DbC
{
    public static class Contract
    {
        public static void Requires(bool condition, string message)
        {
            if (!condition)
            {
                throw new ArgumentException(message);
            }
        }
    }
}