﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Reflection;

namespace Embarr.WebAPI.Validation.Cache
{
    public static class ExpressionToPropertyInfoMemoizer
    {
        static readonly ConcurrentDictionary<Expression, PropertyInfo> ExpressionPropertyInfoMaps = new ConcurrentDictionary<Expression, PropertyInfo>();

        public static Func<Expression, PropertyInfo> Memoize(this Func<Expression, PropertyInfo> function)
        {
            return param =>
            {
                PropertyInfo value;
                if (ExpressionPropertyInfoMaps.TryGetValue(param, out value))
                    return value;
                value = function(param);
                ExpressionPropertyInfoMaps.TryAdd(param, value);
                return value;
            };
        }
    }
}