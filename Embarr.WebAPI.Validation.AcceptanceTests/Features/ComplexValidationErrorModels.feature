﻿@SelfHostWithComplexValidationErrorModels
Feature: ComplexValidationErrorModels
	In order to retrieve a custom validation model
	As a consumer
	I would like to format data annotation validation errors to my custom object

Scenario: Invalid fields return correctly formatted error messages in json
	When I perform a post request with invalid parameters
	Then the response should include the correct complex type validation response

