﻿@SelfHostWithDefaultModelStateValidationModels
Feature: DefaultModelStateValidationModelErrors
	In order to retrieve a custom validation model
	As a consumer
	I would like to format flattened data annotation validation errors to my custom object

Scenario: Invalid fields return correctly formatted error messages in json
	When I perform a post request with invalid parameters
	Then the response should include the correct default model state validation response
