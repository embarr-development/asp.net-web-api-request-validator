﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using Embarr.WebAPI.Validation.AcceptanceTests.Services;
using Embarr.WebAPI.Validation.AcceptanceTests.WebApi.Models;
using Newtonsoft.Json;
using RestSharp;
using Should;
using TechTalk.SpecFlow;

namespace Embarr.WebAPI.Validation.AcceptanceTests.Steps
{
    [Binding]
    public class ValidationErrorsSteps
    {
        [When(@"I perform a post request with invalid parameters")]
        public void WhenIPerformAPostRequestWithInvalidParameters()
        {
            var restClient = new RestClient(FeatureContextService.GetValue<string>("baseAddress"));
            restClient.DefaultParameters.Clear();
            var restRequest = new RestRequest("api/test", Method.POST);
            var testModel = new TestModel {Age = 1};

            restRequest.RequestFormat = DataFormat.Json;
            restRequest.AddHeader("content-type", "application/json");
            restRequest.AddBody(testModel);
            var response = restClient.Execute(restRequest);
            ScenarioContextService.SaveValue("validationResponse", response.Content);
        }

        [Then(@"the response should include the correct complex type validation response")]
        public void ThenTheResponseShouldIncludeTheCorrectComplexTypeValidationResponse()
        {
            var validationResponse = ScenarioContextService.GetValue<string>("validationResponse");
            
            validationResponse.ShouldNotBeNull();

            var validationErrors = JsonConvert.DeserializeObject<List<HierarchalValidationErrors>>(validationResponse);
            validationErrors.Count.ShouldEqual(2);
            validationErrors[0].Property.ShouldEqual("Name");
            validationErrors[0].ErrorMessages.Count.ShouldEqual(1);
            validationErrors[0].ErrorMessages[0].Message.ShouldEqual("The Name field is required.");
            validationErrors[1].Property.ShouldEqual("Age");
            validationErrors[1].ErrorMessages.Count.ShouldEqual(1);
            validationErrors[1].ErrorMessages[0].Message.ShouldEqual("The field Age must be between 10 and 20.");
        }

        [Then(@"the response should include the correct flattened validation response")]
        public void ThenTheResponseShouldIncludeTheCorrectFlattenedValidationResponse()
        {
            var validationResponse = ScenarioContextService.GetValue<string>("validationResponse");

            validationResponse.ShouldNotBeNull();

            var validationErrors = JsonConvert.DeserializeObject<List<FlatErrorMessageModel>>(validationResponse);
            validationErrors.Count.ShouldEqual(2);
            validationErrors[0].Property.ShouldEqual("Name");
            validationErrors[0].ErrorMessage.ShouldEqual("The Name field is required.");
            validationErrors[0].ErrorNumber.ShouldEqual(123);
            validationErrors[1].Property.ShouldEqual("Age");
            validationErrors[1].ErrorMessage.ShouldEqual("The field Age must be between 10 and 20.");
            validationErrors[1].ErrorNumber.ShouldEqual(123);
        }

        [Then(@"the response should include the correct default model state validation response")]
        public void ThenTheResponseShouldIncludeTheCorrectDefaultModelStateValidationResponse()
        {
            var validationResponse = ScenarioContextService.GetValue<string>("validationResponse");

            validationResponse.ShouldNotBeNull();

            validationResponse.ShouldContain("The Name field is required.");
            validationResponse.ShouldContain("The field Age must be between 10 and 20.");
        }
    }
}