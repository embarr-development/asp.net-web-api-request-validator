using System.Web.Http;
using Newtonsoft.Json.Serialization;
using Owin;

namespace Embarr.WebAPI.Validation.AcceptanceTests.WebApi.App_Start
{
    public class DefaultModelStateValidationErrorsStartUp
    {
        public void Configuration(IAppBuilder appBuilder)
        {
            var config = new HttpConfiguration();
            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new {id = RouteParameter.Optional}
                );

            RequestValidator.UseDefaultModelStateFormat(config);

            var serializerSettings =
                config.Formatters.JsonFormatter.SerializerSettings;
            var contractResolver =
                (DefaultContractResolver) serializerSettings.ContractResolver;
            contractResolver.IgnoreSerializableAttribute = true;

            appBuilder.UseWebApi(config);
        }
    }
}