﻿using System.Web.Http;
using Embarr.WebAPI.Validation.AcceptanceTests.WebApi.Models;
using Owin;

namespace Embarr.WebAPI.Validation.AcceptanceTests.WebApi.App_Start
{
    public class ComplexValidationErrorModelsStartUp
    {
        public void Configuration(IAppBuilder appBuilder)
        {
            var config = new HttpConfiguration();
            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            RequestValidator.Create<HierarchalValidationErrors>()
                .MapPropertyName(x => x.Property)
                .MapPropertyErrorMessagesCollection(x => x.ErrorMessages)
                .Map(x => x.Message).ToErrorMessage()
                .Init(config);

            appBuilder.UseWebApi(config);
        }
    }
}