using System.Web.Http;
using Embarr.WebAPI.Validation.AcceptanceTests.WebApi.Models;
using Owin;

namespace Embarr.WebAPI.Validation.AcceptanceTests.WebApi.App_Start
{
    public class FlattenedValidationErrorModelsStartUp
    {
        public void Configuration(IAppBuilder appBuilder)
        {
            var config = new HttpConfiguration();
            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
                );

            RequestValidator.Create<FlatErrorMessageModel>()
                .MapPropertyName(x => x.Property)
                .FlattenErrorMessages()
                .Map(x => x.ErrorMessage).ToErrorMessage()
                .Map(x => x.ErrorNumber).ResolveUsing(MapErrorNumber)
                .Init(config);

            appBuilder.UseWebApi(config);
        }

        private object MapErrorNumber(string arg)
        {
            return 123;
        }
    }
}