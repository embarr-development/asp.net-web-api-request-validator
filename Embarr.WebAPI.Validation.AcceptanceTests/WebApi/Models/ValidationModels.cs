﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Embarr.WebAPI.Validation.AcceptanceTests.WebApi.Models
{
    [Serializable]
    [DataContract]
    public class HierarchalValidationErrors
    {
        [DataMember]
        public string Property { get; set; }

        [DataMember]
        public List<ErrorMessage> ErrorMessages { get; set; }

        public HierarchalValidationErrors()
        {
            ErrorMessages = new List<ErrorMessage>();
        }
    }

    [Serializable]
    [DataContract]
    public class ErrorMessage
    {
        [DataMember]
        public int ErrorNumber { get; set; }

        [DataMember]
        public string Message { get; set; }
    }

    public class FlatErrorMessageModel
    {
        public string Property { get; set; }
        public string ErrorMessage { get; set; }
        public int ErrorNumber { get; set; }
    }
}