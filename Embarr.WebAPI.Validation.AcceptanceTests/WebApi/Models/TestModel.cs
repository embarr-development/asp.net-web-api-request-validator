﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Embarr.WebAPI.Validation.AcceptanceTests.WebApi.Models
{
    public class TestModel
    {
        [Required]
        public string Name { get; set; }
        [Range(10, 20)]
        public int Age { get; set; }
    }
}