﻿using Embarr.WebAPI.Validation.AcceptanceTests.Services;
using Embarr.WebAPI.Validation.AcceptanceTests.WebApi;
using Embarr.WebAPI.Validation.AcceptanceTests.WebApi.App_Start;
using TechTalk.SpecFlow;

namespace Embarr.WebAPI.Validation.AcceptanceTests.Hooks
{
    [Binding]
    public class WebApiHostHooks
    {
        [BeforeFeature("SelfHostWithComplexValidationErrorModels")]
        public static void BeforeComplexHostFeature()
        {
            Host.StartHost<ComplexValidationErrorModelsStartUp>(getBaseAddress: SetBaseAddress);
        }

        [AfterFeature("SelfHostWithComplexValidationErrorModels")]
        public static void AfterComplexHostFeature()
        {
            Host.StopHost();
        }

        [BeforeFeature("SelfHostWithFlattenedValidationModels")]
        public static void BeforeFlattenedHostFeature()
        {
            Host.StartHost<FlattenedValidationErrorModelsStartUp>(getBaseAddress: SetBaseAddress);
        }

        [AfterFeature("SelfHostWithFlattenedValidationModels")]
        public static void AfterFlattenedHostFeature()
        {
            Host.StopHost();
        }

        [BeforeFeature("SelfHostWithDefaultModelStateValidationModels")]
        public static void BeforeDefaultHostFeature()
        {
            Host.StartHost<DefaultModelStateValidationErrorsStartUp>(getBaseAddress: SetBaseAddress);
        }

        [AfterFeature("SelfHostWithDefaultModelStateValidationModels")]
        public static void AfterDefaultHostFeature()
        {
            Host.StopHost();
        }

        private static void SetBaseAddress(string baseAddress)
        {
            FeatureContextService.SaveValue("baseAddress", baseAddress);
        }
    }
}
